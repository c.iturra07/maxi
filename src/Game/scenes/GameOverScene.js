class GameOverScene extends Phaser.Scene {
  constructor() {
    super({ key: "GameOverScene" });
  }

  preload() {
    // Cargar recursos del Game Over aquí (si es necesario)
    this.load.image("gameover", "public/gameover.jpg");
  }

  create() {
    // Mostrar mensaje de Game Over
    this.add.image(710, 300, "gameover");

    // Permitir reiniciar el juego con un clic
    this.input.once("pointerdown", () => {
      this.scene.start("MainScene");
    });
  }
}
export default GameOverScene;
