import Phaser from "phaser";

class GameScene extends Phaser.Scene {
  constructor() {
    super({ key: "game" });
    this.lives = 3;
    this.estaSaltando = false;
    this.coinCount = 0;
  }
  preload() {
    this.load.image("background", "public/background.png");
    this.load.image("gameover", "public/gameover.jpg");
    this.load.image("character", "public/character.png");
    this.load.image("coin", "public/coin.png");
  }

  create() {
    // Ajustar el tamaño del tile sprite según el tamaño de la imagen de fondo
    const backgroundWidth = this.scale.width;
    const backgroundHeight = 600;

    this.background = this.add.tileSprite(0, 0, 5600, 1600, "background");
    this.background.setOrigin(0, 0);
    this.physics.world.setBounds(500, 100, 5600, 1200);

    this.gameoverImage = this.add.image(400, 90, "gameover");
    this.gameoverImage.visible = false;

    this.character = this.physics.add.image(500, 320, "character");
    this.character.setCollideWorldBounds(true);

    //this.coin = this.physics.add.image(500, 320, "coin");
    //this.coin.setCollideWorldBounds(true);

    // Crear un grupo de monedas
    this.coins = this.physics.add.group({
      key: "coin",
      repeat: 10,
      setXY: { x: 12, y: 0, stepX: 70 },
    });
    // Posicionar aleatoriamente las monedas
    this.coins.children.iterate(function (coin) {
      coin.setX(Phaser.Math.Between(50, backgroundWidth - 50));
      coin.setY(Phaser.Math.Between(50, backgroundHeight - 50));
      coin.setCollideWorldBounds(true);
    });
    // Colision entre character y moneda
    this.physics.add.overlap(
      this.character,
      this.coin,
      this.collectCoin,
      null,
      this
    );

    this.cursors = this.input.keyboard.createCursorKeys();
    this.shiftKey = this.input.keyboard.addKey(
      Phaser.Input.Keyboard.KeyCodes.SHIFT
    );
    this.spaceKey = this.input.keyboard.addKey(
      Phaser.Input.Keyboard.KeyCodes.SPACE
    );
    this.cameras.main.startFollow(this.character);

    this.livesText = this.add.text(16, 16, "Vidas: " + this.lives, {
      fontSize: "32px",
      fill: "#fff",
    });
    this.coinCount = this.add.text(16, 50, "Monedas: " + this.coinCount, {
      fontSize: "32px",
      fill: "#fff",
    });
    this.livesText.setScrollFactor(0);
    this.coinCount.setScrollFactor(0);

    // Ajustar gravedad para que el personaje caiga al suelo
    this.character.setGravityY(1000);
  }

  update() {
    if (this.shiftKey.isDown) {
      if (this.cursors.left.isDown) {
        this.character.setVelocityX(-1000);
      } else if (this.cursors.right.isDown) {
        this.character.setVelocityX(1000);
      } else {
        this.character.setVelocityX(0);
        this.character.setVelocityY(0);
      }
    } else if (!this.shiftKey.isDown) {
      if (this.cursors.left.isDown) {
        this.character.setVelocityX(-500);
      } else if (this.cursors.right.isDown) {
        this.character.setVelocityX(500);
      } else {
        this.character.setVelocityX(0);
        this.character.setVelocityY(0);
      }
    } else {
      this.character.setVelocityX(0);
      this.character.setVelocityY(0);
    }
    if (this.cursors.up.isDown && !this.estaSaltando) {
      this.estaSaltando = true;
      this.character.setVelocityY(-4000);
      this.time.delayedCall(
        500,
        () => {
          this.estaSaltando = false;
        },
        [],
        this
      );
    } else if (this.cursors.down.isDown) {
      //this.character.setVelocityY(500);
      this.loseLife();
      //this.scene.start("GameOverScene");
    }
    if (this.spaceKey.isDown && this.cursors.left.isDown) {
      this.teletransportarIzq();
    } else if (this.spaceKey.isDown) {
      this.teletransportarDer();
    }
  }
  teletransportarIzq() {
    const teletransporteDistancia = 100;
    this.character.x -= teletransporteDistancia;
  }
  teletransportarDer() {
    const teletransporteDistancia = 100;
    this.character.x += teletransporteDistancia;
  }
  enTierra() {
    this.estaSaltando = false; // Marcar que el personaje ha aterrizado
  }
  loseLife() {
    this.lives -= 1;
    this.livesText.setText("Vidas: " + this.lives);

    if (this.lives <= 0) {
      this.scene.start("GameOverScene");
    } else {
      this.character.setPosition(500, 320);
    }
  }
  gameOver() {
    this.gameoverImage.visible = true;
    this.physics.pause();
    this.character.setTint(0xff0000);
    this.character.anims.stop(); //
    this.time.delayedCall(
      2000,
      () => this.scene.start("GameOverScene"),
      [],
      this
    );
  }
  collectCoin(character, coin) {
    coin.disableBody(true, true);
    this.coinCount += 1;
    console.log(`Monedas recolectadas: ${this.coinCount}`);
  }
}
export default GameScene;
