export class Game extends Phaser.Scene {
  constructor() {
    super({ key: "game" });
  }

  preload() {
    this.load.image("background", "public/background.png");
    this.load.image("gameover", "public/gameover.jpg");
    this.load.image("character", "public/character.png");
    this.load.image("coin", "public/coin.png");
  }

  create() {
    this.physics.world.setBoundsCollision(false, false, false, true);
    this.add.image(410, 250, "background");
    this.gameoverImage = this.add.image(400, 90, "gameover");
    this.gameoverImage.visible = false;
    this.character = this.physics.add.image(500, 320, "character");
    this.character.setCollideWorldBounds(true);
    //this.coin = this.physics.add.image(500, 320, 'coin');
    //this.coin.setCollideWorldBounds(true);
    this.cursors = this.input.keyboard.createCursorKeys();
  }
  update() {
    if (this.cursors.left.isDown) {
      this.character.setVelocityX(-500);
    } else if (this.cursors.right.isDown) {
      this.character.setVelocityX(500);
    } else if (this.cursors.up.isDown) {
      this.character.setVelocityY(-500);
    } else if (this.cursors.down.isDown) {
      this.character.setVelocityY(500);
    } else {
      this.character.setVelocityX(0);
      this.character.setVelocityY(0);
    }
  }
}
