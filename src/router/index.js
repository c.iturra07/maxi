import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: "/",
            name: "inicio",
            component: () => import("../views/HomeView.vue")
        },
        {
            path: "/game",
            name: "game",
            component: () => import("../views/GameView.vue"),
        },
        {
            path: "/ranking",
            name: "ranking",
            component: () => import("../views/RankingView.vue")
        }
    ]
})

export default router;