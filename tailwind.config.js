/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx,vue}"],
  theme: {
    extend: {
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
        arco: ["Arco", "sans-serif"],
      },
      backgroundImage: {
        home: "url('public/home.jpg')",
      },
    },
  },
  plugins: [],
};
